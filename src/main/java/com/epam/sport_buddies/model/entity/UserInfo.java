package com.epam.sport_buddies.model.entity;

import com.epam.sport_buddies.model.SportType;

import javax.persistence.*;

@Entity
@Table(name = "userInfo")
public class UserInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "height")
    private Double height;

    @Column(name = "weight")
    private Double weight;

    @Column(name = "sport_type")
    private SportType sportType;

    @Column(name = "age")
    private Integer age;

    @OneToOne(mappedBy = "userInfo")
    private User user;

}
