package com.epam.sport_buddies.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "users")
public class User {


    @PrePersist
    private void init() {
        createDate = new Date();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "second_name")
    private String secondName;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "active")
    private Boolean active;

    @Column(name = "create_date")
    private Date createDate;

    @OneToOne
    @JoinColumn(name = "userInfo_id")
    private UserInfo userInfo;

    @ManyToOne
    @JoinColumn(name = "address_id")
    private Address address;

}
