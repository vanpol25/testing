package com.epam.sport_buddies.model;

public enum SportType {

    WALKING, RUNNING, SWIMMING, CYCLING, TENIS, FOOTBALL, YOGA, GYM_TRAINING

}
