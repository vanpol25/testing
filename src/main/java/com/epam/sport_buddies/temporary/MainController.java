package com.epam.sport_buddies.temporary;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MainController {

    @Autowired
    private EmailService emailService;

    @PostMapping("/sendMessage")
    public String sendMessage(@RequestParam String email, @RequestParam String message) {
        emailService.sendEmail(email, message);
        return "redirect:/";
    }
}
