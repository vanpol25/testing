package com.epam.sport_buddies;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SportBuddiesApplication {

    public static void main(String[] args) {
        SpringApplication.run(SportBuddiesApplication.class, args);
    }

}
